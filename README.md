## Cardiovascular Disease Dataset

## Description

Les données brutes représentées nous permettront de détecter les maladies cardiaques en fonction de plusieurs caractéristiques : âge, tabagisme, cholestérol.. avec un Target : cardio (binaire)

Objectif : informations factuelles ;

Examen : résultats de l'examen médical ;

Subjectif : informations données par le patient.
___
Features:

Age | Objective Feature | age | int (days)|

Height | Objective Feature | height | int (cm) 

Weight | Objective Feature | weight | float (kg) |

Gender | Objective Feature | gender | categorical code |

Systolic blood pressure | Examination Feature | ap_hi | int |

Diastolic blood pressure | Examination Feature | ap_lo | int |

Cholesterol | Examination Feature | cholesterol | 1: normal, 2: above normal, 3: well above normal |

Glucose | Examination Feature | gluc | 1: normal, 2: above normal, 3: well above normal |

Smoking | Subjective Feature | smoke | binary |

Alcohol intake | Subjective Feature | alco | binary |

Physical activity | Subjective Feature | active | binary |

Presence or absence of cardiovascular disease | 

Target Variable | cardio | binary |
___
Toutes les valeurs de l'ensemble de données ont été recueillies au moment de l'examen médical.

___
# Description des variables 

| variable | type | short label | long label |
| ------ | ------ |------ |------ |
|    age |   quanti     |Age|Age (days) |
|    height|      quanti  |Height|Height|
|weight|quanti|Weight|Weight|
|gender|quali|Gender|Gender|
|ap_hi|quanti|Sys blood pressure|Systolic blood pressure|
|ap_lo|quanti|Dias blood pressure|Diastolic blood pressure|
|cholesterol|quali|cholesterol| Cholesterol|
|gluc|quali|Gluco|Glucose|
|smoke|binaire|Smoking|Smoking|
|alco|binaire|Alcohol|Alcohol intake|
|active|binaire|Activity|Physical activity|
|cardio|binaire|Cardiovascular disease|Presence or absence of cardiovascular disease|

# Data management 

Transformer la variable de type age (jours) en years (année) et la stocker dans une autre variable. 

# Licence et conditions de réutilisation

Licence ouvert Etalab v2.0


# Données brutes

**Accéder aux données brutes** https://www.kaggle.com/datasets/sulianova/cardiovascular-disease-dataset

# Références

(1) https://www.kaggle.com/datasets
